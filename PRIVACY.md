UUID Bot Privacy Statement
==

UUID Bot does not store any information about you for long periods of time, and
only temporarily stores your user ID, for no more than a few seconds. Your User
ID is a number that identifies you uniquely, and it is stored so that UUID Bot 
can reply to your message.