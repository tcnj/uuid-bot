var restify = require('restify');
var builder = require('botbuilder');
var uuid = require('uuid');

//=========================================================
// Bot Setup
//=========================================================

// Setup Restify Server
var server = restify.createServer();
server.listen(process.env.port || process.env.PORT || 3978, function () {
   console.log('%s listening to %s', server.name, server.url); 
});
  
// Create chat bot
var connector = new builder.ChatConnector({
    appId: process.env.MICROSOFT_APP_ID,
    appPassword: process.env.MICROSOFT_APP_PASSWORD
});
var bot = new builder.UniversalBot(connector);
server.post('/api/messages', connector.listen());

//=========================================================
// Bots Dialogs
//=========================================================

var uuidReg = /([0-9]+) uuids?/i;

bot.dialog('/', [
    function(session, args, next){
        if(session.message && session.message.text){
            var match = uuidReg.exec(session.message.text);
            if(match == null){
                session.send("Hi. I'm not really here to chat y'know? I'm just here to generate UUIDs. Try: \"Can I have 5 UUIDs?\"");
                return;
            }
            var num = parseInt(match[0]);
            if(num > 100){
                session.send("Whoa! Sorry, but currently there is a limit of 100 UUIDs at once, as per the fair usage policy. Sorry to spoil your fun. You can find the latest version of the fair usage policy here: https://gitlab.com/tcnj/uuid-bot/blob/master/USE.md");
                return;
            }
            for(var i = 0; i < num; i++){
                session.send(uuid.v4());
            }
        }else{
            session.send("Hi. I'm not really here to chat y'know? I'm just here to generate UUIDs. Try: \"Can I have 5 UUIDs?\"");
        }
    }
]);