Terms Of Use
==

Your use of UUID Bot ("The Bot") is conditional to fair usage. Currently,
Fair usage is defined as not requesting past the limit of 100 UUIDs at
once. I, Danny Wensley ("The Owner"), reserve the right to widthdraw the 
service at any time, and to also block specific users or groups of users 
from the service at my sole discreation.

The Owner does not accept any responsibility for any damage that is caused
by using this tool, to either your computer, or any relationships between
you and those involved, to the extent possible by applicable law.

You may only use this service if you accept these terms. You may widthdraw
from these terms at any time, by simply not using the service.